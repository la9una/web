# Primeros pasos

Comenzar a trabajar con Bootstrap es muy sencillo, simplemente debemos descargar los archivos del proyecto y configurar nuestra plantilla inicial para comenzar a trabajar. 

!!! info "Versiones utilizadas"
		Las versiones de Bootstrap se actualizan constantemete, al igual que los componentes que esta herramienta utiliza. Al momento de escribir esta guía se emplean las siguientes versiones: 

		* [Bootstrap](https://getbootstrap.com/): 4.1
		* [jQuery](https://jquery.com/): 3.3.1
		* [Popper.js](https://popper.js.org/): 1.14.3

## ¿Que haremos en esta sección?
Partiremos de la plantilla necesaria con Bootstrap integrado. A partir de ésta, crearemos una página de inicio dejando todo listo para comenzar a trabajar con Bootstrap. 

!!!done "Plantilla Bootstrap"
        Para comenzar, emplearemos el ["Starter Template" proporcionado por Bootstrap](https://getbootstrap.com/docs/5.0/getting-started/introduction/#starter-template)

## Creando nuestra primera página

Partiendo del "Starter Template", crearemos un archivo HTML llamdo `index.html`. Con este simple paso tendremos todo lo necesario para comenzar nuestro proyecto web basándonos en el _framework_ Bootrap. 





