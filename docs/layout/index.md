Ejemplo de layuot responsive básico con _flexbox_ empleando Bootstrap

```html linenums="1" hl_lines="17 18 19 20 21"
<!DOCTYPE html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Bootstrap CSS -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
      crossorigin="anonymous"
    />
    <title>Layout Responsive</title>
  </head>
  <body>
    <div class="d-flex flex-column flex-sm-wrap vh-100 vw-100">
      <div class="col-sm-8 col-12 h-100">CONTENIDO PRINCIPAL</div>
      <div class="col-sm-4 col-12 h-50">LATERAL ARRIBA</div>
      <div class="col-sm-4 col-12 h-50">LATERAL ABAJO</div>
    </div>
    <!-- JavaScript -->
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
      crossorigin="anonymous"
    ></script>
  </body>
</html>
```
